/// <reference types="Cypress" />

context('Viewport', () => {
    beforeEach(() => {
      cy.visit('https://getbootstrap.com/docs/4.3/components/forms/')
    })

    it('Automated Login Form Filling', () => {
      cy.get('#exampleInputEmail1').type('Hackathon_2019@harman.com',{delay: 10});
      cy.get('#exampleInputPassword1').type('Iamahackathonparticipant',{delay: 10});
      cy.get('#exampleCheck1').check();
      cy.get(':nth-child(9) > form > .btn').click();
    })

    it('Automated Form Controls demo', () => {
      cy.get('#exampleFormControlInput1').type('Hackathon_2019@harman.com',{delay: 10});
      cy.get('#exampleFormControlSelect1').select('4');
      cy.get('#exampleFormControlSelect2').select(['3','5']).invoke('val');
      cy.get('#exampleFormControlTextarea1').type('Hey this is a demo to show how ease it is to automate and test !!');
    });

    it('Automated File selection', () => {
      cy.get('#exampleFormControlFile1').select
    });

  })
  