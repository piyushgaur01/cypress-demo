const bootstrap = 'https://getbootstrap.com';

describe('Check Navbar Elements' , () => {
    it('Browse to bootstrap Admin UI' , () => {
     cy.Login(bootstrap);
     cy.title()
       .should('eq', 'Bootstrap · The most popular HTML, CSS, and JS library in the world.');
     cy.get('.navbar')
       .should('exist');
     cy.get('.d-block')
       .parent()
       .should('exist')
       .should('have.attr', 'href', '/')
     cy.get('.navbar-nav-scroll > .navbar-nav > :nth-child(1) > .nav-link')
        .should('exist')
        .should('have.attr', 'href', '/')
        .should('contain', 'Home')
     cy.get('.navbar-nav-scroll > .navbar-nav > :nth-child(2) > .nav-link')
        .should('exist')
        .should('have.attr', 'href', '/docs/4.3/getting-started/introduction/')
        .should('contain', 'Documentation')
    cy.get('.navbar-nav-scroll > .navbar-nav > :nth-child(3) > .nav-link')
        .should('exist')
        .should('have.attr', 'href', '/docs/4.3/examples/')
        .should('contain', 'Examples')
    cy.get('.navbar-nav-scroll > .navbar-nav > :nth-child(4) > .nav-link')
        .should('exist')
        .should('have.attr', 'href', 'https://themes.getbootstrap.com/')
        .should('contain', 'Themes')
    cy.get('.navbar-nav-scroll > .navbar-nav > :nth-child(5) > .nav-link')
        .should('exist')
        .should('have.attr', 'href', 'https://expo.getbootstrap.com/')
        .should('contain', 'Expo');
    cy.get('.navbar-nav-scroll > .navbar-nav > :nth-child(6) > .nav-link')
        .should('exist')
        .should('have.attr', 'href', 'https://blog.getbootstrap.com/')
        .should('contain', 'Blog');
    })
})