/// <reference types="Cypress" />

context('Viewport', () => {
    beforeEach(() => {
      cy.visit('https://getbootstrap.com/docs/4.3/components/modal/')
    })
  
    it('Test Modal', () => {
        cy.get(':nth-child(21) > .btn').click();
        expect(cy.get('#exampleModalLive')).to.have.class('show');
    })
  })
  