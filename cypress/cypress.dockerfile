FROM cypress/base:10

COPY ["package*.json", "/usr/src/app/"]
WORKDIR /usr/src/app
RUN npm install cypress async

COPY . /usr/src/app
CMD npm test