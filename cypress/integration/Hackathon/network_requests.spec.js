/// <reference types="Cypress" />

context('Network Requests', () => {
  beforeEach(() => {
    cy.visit('https://example.cypress.io/commands/network-requests')
  })

  

  it('cy.server() - control behavior of network requests and responses', () => {
    

    cy.server().should((server) => {
      
      
      expect(server.delay).to.eq(0)
      expect(server.method).to.eq('GET')
      expect(server.status).to.eq(200)
      expect(server.headers).to.be.null
      expect(server.response).to.be.null
      expect(server.onRequest).to.be.undefined
      expect(server.onResponse).to.be.undefined
      expect(server.onAbort).to.be.undefined

      
      

      
      expect(server.enable).to.be.true
      
      expect(server.force404).to.be.false
      
      expect(server.whitelist).to.be.a('function')
    })

    cy.server({
      method: 'POST',
      delay: 1000,
      status: 422,
      response: {},
    })

    
    
    
  })

  it('cy.request() - make an XHR request', () => {
    
    cy.request('https://jsonplaceholder.cypress.io/comments')
      .should((response) => {
        expect(response.status).to.eq(200)
        expect(response.body).to.have.length(500)
        expect(response).to.have.property('headers')
        expect(response).to.have.property('duration')
      })
  })


  it('cy.request() - verify response using BDD syntax', () => {
    cy.request('https://jsonplaceholder.cypress.io/comments')
    .then((response) => {
      
      expect(response).property('status').to.equal(200)
      expect(response).property('body').to.have.length(500)
      expect(response).to.include.keys('headers', 'duration')
    })
  })

  it('cy.request() with query parameters', () => {
    
    
    cy.request({
      url: 'https://jsonplaceholder.cypress.io/comments',
      qs: {
        postId: 1,
        id: 3,
      },
    })
    .its('body')
    .should('be.an', 'array')
    .and('have.length', 1)
    .its('0') 
    .should('contain', {
      postId: 1,
      id: 3,
    })
  })

  it('cy.route() - route responses to matching requests', () => {
    

    let message = 'whoa, this comment does not exist'

    cy.server()

    
    cy.route('GET', 'comments/*').as('getComment')

    
    
    cy.get('.network-btn').click()

    
    cy.wait('@getComment').its('status').should('eq', 200)

    
    cy.route('POST', '/comments').as('postComment')

    
    
    cy.get('.network-post').click()
    cy.wait('@postComment')

    
    cy.get('@postComment').should((xhr) => {
      expect(xhr.requestBody).to.include('email')
      expect(xhr.requestHeaders).to.have.property('Content-Type')
      expect(xhr.responseBody).to.have.property('name', 'Using POST in cy.route()')
    })

    
    cy.route({
      method: 'PUT',
      url: 'comments/*',
      status: 404,
      response: { error: message },
      delay: 500,
    }).as('putComment')

    
    
    cy.get('.network-put').click()

    cy.wait('@putComment')

    
    cy.get('.network-put-comment').should('contain', message)
  })
})
