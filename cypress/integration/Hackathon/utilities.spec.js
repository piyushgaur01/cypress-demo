/// <reference types="Cypress" />

context('Utilities', () => {
  beforeEach(() => {
    cy.visit('https://example.cypress.io/utilities')
  })

  it('Cypress._ - call a lodash method', () => {
    
    cy.request('https://jsonplaceholder.cypress.io/users')
      .then((response) => {
        let ids = Cypress._.chain(response.body).map('id').take(3).value()

        expect(ids).to.deep.eq([1, 2, 3])
      })
  })

  it('Cypress.$ - call a jQuery method', () => {
    
    let $li = Cypress.$('.utility-jquery li:first')

    cy.wrap($li)
      .should('not.have.class', 'active')
      .click()
      .should('have.class', 'active')
  })

  it('Cypress.Blob - blob utilities and base64 string conversion', () => {
    
    cy.get('.utility-blob').then(($div) =>
    
    
      Cypress.Blob.imgSrcToDataURL('https://example.cypress.io/assets/img/javascript-logo.png', undefined, 'anonymous')
      .then((dataUrl) => {
        
        let img = Cypress.$('<img />', { src: dataUrl })

        
        
        
        $div.append(img)

        cy.get('.utility-blob img').click()
          .should('have.attr', 'src', dataUrl)
      }))
  })

  it('Cypress.minimatch - test out glob patterns against strings', () => {
    
    let matching = Cypress.minimatch('/users/1/comments', '/users/*/comments', {
      matchBase: true,
    })

    expect(matching, 'matching wildcard').to.be.true

    matching = Cypress.minimatch('/users/1/comments/2', '/users/*/comments', {
      matchBase: true,
    })
    expect(matching, 'comments').to.be.false

    // ** matches against all downstream path segments
    matching = Cypress.minimatch('/foo/bar/baz/123/quux?a=b&c=2', '/foo/**', {
      matchBase: true,
    })
    expect(matching, 'comments').to.be.true

    

    matching = Cypress.minimatch('/foo/bar/baz/123/quux?a=b&c=2', '/foo/*', {
      matchBase: false,
    })
    expect(matching, 'comments').to.be.false
  })


  it('Cypress.moment() - format or parse dates using a moment method', () => {
    
    const time = Cypress.moment().utc('2014-04-25T19:38:53.196Z').format('h:mm A')

    expect(time).to.be.a('string')

    cy.get('.utility-moment').contains('3:38 PM')
      .should('have.class', 'badge')

    
    const start = Cypress.moment('3:00 PM', 'LT')
    const end = Cypress.moment('5:00 PM', 'LT')

    cy.get('.utility-moment .badge')
      .should(($el) => {
        
        const m = Cypress.moment($el.text().trim(), 'LT')

        
        const f = 'h:mm A'

        expect(m.isBetween(start, end),
          `${m.format(f)} should be between ${start.format(f)} and ${end.format(f)}`).to.be.true
      })
  })


  it('Cypress.Promise - instantiate a bluebird promise', () => {
    
    let waited = false

    /**
     * @return Bluebird<string>
     */
    function waitOneSecond () {
      
      // @ts-ignore TS2351 (new Cypress.Promise)
      return new Cypress.Promise((resolve, reject) => {
        setTimeout(() => {
          
          waited = true

          
          resolve('foo')
        }, 1000)
      })
    }

    cy.then(() =>
    
    
      // @ts-ignore TS7006
      waitOneSecond().then((str) => {
        expect(str).to.eq('foo')
        expect(waited).to.be.true
      }))
  })
})
