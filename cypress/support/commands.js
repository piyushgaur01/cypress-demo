// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
Cypress.Commands.add('Login', (url) => {
    cy.window().then((win) => {
      cy.visit(url);
    //   cy.get('[data-cy=username]', { timeout: 60000 }).type(id);
    //   cy.get('[data-cy=password]', { timeout: 60000 }).type(password);
    //   cy.get('[data-cy=login]', { timeout: 60000 }).click();
      win.sessionStorage.clear();
    });
  });